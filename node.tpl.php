<?php
?>
<article id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

  <figure><?php print $picture ?></figure>

  <?php if ($page == 0): ?>
    <header><h2 class="teaser-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2></header>
  <?php endif; ?>

  <?php if ($submitted): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <section class="content">
    <?php print $content ?>
  </section>

  <?php if ($links||$taxonomy){ ?>
    <section class="meta">

      <?php if ($links): ?>
        <section class="links">
          <?php print $links; ?>
        </section>
      <?php endif; ?>

      <?php if ($taxonomy): ?>
        <section class="terms">
          <?php print $terms ?>
        </section>
      <?php endif;?>

      <span class="clear"></span>

    </section>
  <?php }?>

</article>
