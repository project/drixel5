<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" xmlns:fb="http://www.facebook.com/2008/fbml">
  <head>
  <meta charset=utf-8>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
		<!--[if IE]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
    <?php print $styles ?>
    <?php print $scripts ?>

    <!--[if lte IE 7]><?php print phptemplate_get_ie_styles(); ?><![endif]--><!--If Less Than or Equal (lte) to IE 7-->
  </head>
  <body<?php print phptemplate_body_class($left, $right); ?>>
<?php if (!empty($admin)) print $admin; ?>
<!-- Layout -->
    <div id="wrapper">
      <header id="page-header">
        <?php print $header; ?>
        <?php if ($logo): ?>
          <figure id="logo"><a href="<?php print check_url($front_page); ?>" title="<?php print check_plain($site_name); ?>">
            <img src="<?php print check_url($logo); ?>" alt="<?php print check_plain($site_name); ?>" />
          </a></figure>
        <?php endif; ?>
        <?php print '<h1><a href="'. check_url($front_page) .'" title="'. check_plain($site_name) .'">';
          if ($site_name) {
            print '<span id="sitename">'. check_plain($site_name) .'</span>';
          }
          if ($site_slogan) {
            print '<span id="siteslogan">'. check_plain($site_slogan) .'</span>';
          }
          print '</a></h1>';
        ?>
        <?php if ($search_box): ?><?php print $search_box ?><?php endif; ?>
        <div class="clear"></div>
      </header> <!-- /#header -->
	  <div id="nav-container">
      <nav>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
        <?php endif; ?>
      </nav> <!-- /#nav -->
	  <div id="RSS-Container">
	  <?php print $feed_icons ?>
	  </div>
	  </div>
      <div id="container">
	    <section id="container-header"></section>
        <section id="center">
		  <?php print $breadcrumb; ?>
          <?php if ($mission): print '<section id="mission">'. $mission .'</section>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
		  <?php if ($content_top): ?>        
		    <section id="content-top">
			  <?php print $content-top ?>
			</section>
		  <?php endif; ?>	
          <?php print $content ?>
		</section>
 <!-- /#center -->  
        <?php if ($right): ?>
        <aside id="sidebar-right" class="sidebar">
          <?php print $right ?>
        </aside> <!-- /#sidebar-right -->
        <?php endif; ?>
		<!-- footer --> 
        <footer class="clear">
          
		  <?php if ($footer): ?>
		    <section id="primary-footer" class="footer-section"><?php print $footer_message . $footer ?></section>
		  <?php endif; ?>
		  
		  <section id="3column-footer" class="footer-section">
		  <?php if ($footer1): ?>
		    <section id="footer1" class="columns"><?php print $footer1 ?></section>
		  <?php endif; ?>
		  <?php if ($footer2): ?>
		    <section id="footer2" class="columns"><?php print $footer2 ?></section>
		  <?php endif; ?>
		  <?php if ($footer3): ?>
		    <section id="footer3" class="columns"><?php print $footer3 ?></section>
		  <?php endif; ?>
		    <div class="clear"></div>
		  </section>
		  
		  <?php if ($secondary_footer): ?>
		  <section id="secondary-footer" class="footer-section">
			<?php print $secondary_footer ?>
		  </section>
		  <?php endif; ?>  
		  
		  
		  <section id="2column-footer-wide-right" class="footer-section">
		    <?php if ($footer4): ?>
			  <section id="footer4" class="columns"><?php print $footer4 ?></section>
		    <?php endif; ?>
		    <?php if ($footer5): ?>
		      <section id="footer5" class="columns"><?php print $footer5 ?></section>
		    <?php endif; ?>
			<div class="clear"></div>
		  </section>
		  
		  <section id="2column-footer-wide-left" class="footer-section">
		    <?php if ($footer6): ?>			
			  <section id="footer6" class="columns"><?php print $footer6 ?></section>
		    <?php endif; ?>			
			<?php if ($footer7): ?>
		      <section id="footer7" class="columns"><?php print $footer7 ?></section>
			<?php endif; ?>
			  <div class="clear"></div>
		  </section>
			
		  <?php if ($tertiary_footer): ?>
		  <section id="tertiary-footer" class="footer-section">
			<?php print $tertiary_footer ?>
		  </section>
		  <?php endif; ?>  
			
          
        </footer>
		<!-- /#footer -->
      </div> <!-- /#container -->
      <span class="clear"></span>
    </div> <!-- /#wrapper -->
<!-- /layout -->
  <?php print $closure ?>
  </body>
</html>
