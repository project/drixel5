<article class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ' '. $status; print ' '. $zebra; ?>">

  <section class="comment-bar">
    <?php if ($submitted): ?>
      <span class="submitted"><?php print $submitted; ?></span>
    <?php endif; ?>

    <?php if ($comment->new) : ?>
      <span class="new"><?php print drupal_ucfirst($new) ?></span>
    <?php endif; ?>
  </section>

  <figure><?php print $picture ?></figure>

  <header><h3><?php print $title ?></h3></header>
  
  <section class="content">
    <?php print $content ?>
    <?php if ($signature): ?>
      <div>—</div>
      <?php print $signature ?>
    <?php endif; ?>
  </section>

  <?php if ($links): ?>
    <section class="links">
      <?php print $links ?>
    </section>
  <?php endif; ?>

</article>
