HTML 5 Theme by Steve J Bayer based on the Pixel (formerly known as Pixeled) Wordpress theme by http://samk.ca

Initial XHTML Drupal conversion of Pixel was built using the Framework theme by Andre Griffen.

Pixel has been renamed to Drixel since it has Drupal specific styling by Steve J Bayer.

Support for the following modules/features have code contributed by:
Image_FUpload(Mark_Watson27)
RTL Support (tsi)